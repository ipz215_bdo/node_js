require("dotenv").config()
const mongourl = process.env.MONGO_URL
const PORT =  process.env.PORT
let url = `http://localhost:${PORT}`;

const users = [
    {name:"Dima", age:20, email:"4asnykgamer@gmail.com", password:"12345678", phone: "+380995336538"},
    {name:"Dima2", age:20, email:"dimaruli473@gmail.com", password:"12345678", phone: "+380995336538"},
];

const tasks = [
    {title:"Task1", description:"Task1 description"},
    {title:"Task2", description:"Task2 description"},
];


let mongoose = require("mongoose");
mongoose.connect(mongourl);

let User = require('../src/Models/Users.model');
let Task = require('../src/Models/Task.model');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

let token = "";
let id = "";

describe('TaskApp', () => {
    before(async () => {
        await User.deleteMany({});
        await Task.deleteMany({});
    });


    describe('Scenario #1', () => {
        it('User1 create (success)', function (done) {
            chai.request(url)
                .post('/users/registration')
                .send(users[0])
                .end((err, res) => {
                    res.should.have.status(201);
                    done();
                    console.log(`Status: ${res.status}, Message: ${res.body.message}`);
                });
        });

        it('User1 create (failure)', function (done) {
            chai.request(url)
                .post('/users/registration')
                .send(users[0])
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                    console.log(`Status: ${res.status}, Message: ${res.body.message}`);
                });
        });

        it('User2 create (success)', function (done) {
            chai.request(url)
                .post('/users/registration')
                .send(users[1])
                .end((err, res) => {
                    res.should.have.status(201);
                    done();
                    console.log(`Status: ${res.status}, Message: ${res.body.message}`);
                });
        });

        it('User1 login (success)', function (done) {
            chai.request(url)
                .post('/users/login')
                .send({ email: users[0].email, password: users[0].password })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('token');
                    token = res.body.token;
                    done();
                    console.log(`Status: ${res.status}`);
                });
        });

        it('Task1 create', function (done) {
            chai.request(url)
                .post('/tasks/create')
                .send(tasks[0])
                .set("Authorization", `Bearer ${token}`)
                .end((err, res) => {
                    res.should.have.status(201);
                    id = res.body._id;
                    done();
                    console.log(`Status: ${res.status}`);
                });
        });

        it('Task2 create', function (done) {
            chai.request(url)
                .post('/tasks/create')
                .send(tasks[1])
                .set("Authorization", `Bearer ${token}`)
                .end((err, res) => {
                    res.should.have.status(201);
                    done();
                    console.log(`Status: ${res.status}`);
                });
        });

        it('Get all task', function (done) {
            chai.request(url)
                .get('/tasks/All')
                .set("Authorization", `Bearer ${token}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                    console.log(`Status: ${res.status}`);
                });
        });

        it('Get task by id', function (done) {
            chai.request(url)
                .get(`/tasks/${id}`)
                .set("Authorization", `Bearer ${token}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                    console.log(`Status: ${res.status}`);
                });
        });


        it('User1 logout', function (done) {
            chai.request(url)
                .post('/users/logout')
                .set("Authorization", `Bearer ${token}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                    console.log(`Status: ${res.status}`);
                });
        });

        it('User2 login', function (done) {
            chai.request(url)
                .post('/users/login')
                .send({ email: users[1].email, password: users[1].password })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('token');
                    token = res.body.token;
                    done();
                    console.log(`Status: ${res.status}`);
                });
        });

        it('User2 add task3', function (done) {
            chai.request(url)
                .post('/tasks/create')
                .send({title:"Task3", description:"Task3 description"})
                .set("Authorization", `Bearer ${token}`)
                .end((err, res) => {
                    res.should.have.status(201);
                    done();
                    console.log(`Status: ${res.status}`);
                });
        });

        it('User2 get tasks', function (done) {
            chai.request(url)
                .get('/tasks/All')
                .set("Authorization", `Bearer ${token}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                    console.log(`Status: ${res.status}`);
                });
        });

        it('Task1 get', function (done) {
            chai.request(url)
                .get(`/tasks/${id}`)
                .set("Authorization", `Bearer ${token}`)
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                    console.log(`Status: ${res.status}, Message: ${res.body.message}`);
                });
        });

        it('User2 logout', function (done) {
            chai.request(url)
                .post('/users/logout')
                .set("Authorization", `Bearer ${token}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                    console.log(`Status: ${res.status}`);
                });
        });

        it('Task1 get', function (done) {
            chai.request(url)
                .get(`/tasks/${id}`)
                .set("Authorization", `Bearer ${token}`)
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                    console.log(`Status: ${res.status}, Message: ${res.body.message}`);
                });
        });
    });
});