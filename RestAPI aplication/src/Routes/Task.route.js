const express = require('express');
const router = new express.Router();
const taskController = require('../Controller/Task.controller');
const userMiddleware = require('../Middleware/user.middleware');

router.post('/create', userMiddleware, taskController.createTask);
router.get('/all',userMiddleware, taskController.getTasks);
router.patch('/:id',userMiddleware, taskController.updateTask);
router.delete('/:id',userMiddleware, taskController.deleteTask);
router.get('/:id',userMiddleware, taskController.getTaskById);
router.delete('/all',userMiddleware, taskController.deleteAllTasks);

module.exports = router;