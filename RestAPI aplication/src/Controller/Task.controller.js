const Task = require('../Models/Task.model');

class taskController {

    async createTask(req, res) {
        try {
            const { title, description, completed} = req.body;
            const user = req.user;
            const task = new Task({ title, description, completed, user: user.id });
            await task.save();
            return res.status(201).json(task);
        } catch (e) {
            return res.status(500).json(e);
        }
    }

    async getTasks(req, res) {
        try {
            const user = req.user;
            const tasks = await Task.find({ user: user.id });

            return res.status(200).json(tasks);
        } catch (e) {
            return res.status(500).json(e);
        }
    }

    async deleteAllTasks(req, res) {
        try {
            const user = req.user;
            await Task.deleteMany({ user: user.id });
            return res.status(200).json({ message: 'All tasks deleted!' });
        } catch (e) {
            return res.status(500).json(e);
        }
    }

    async getTaskById(req, res) {
        try {
            const id = req.params.id;
            const user = req.user;
            const task = await Task.findOne({ _id: id, user: user.id });
            if (!task) {
                return res.status(404).json({ message: 'Task not found!' });
            }
            return res.status(200).json(task);
        } catch (e) {
            return res.status(500).json(e);
        }
    }

    async updateTask(req, res) {
        try {
            const id = req.params.id;
            const updates = req.body;
            const user = req.user;
            const result = await Task.findOneAndUpdate({ _id: id, user: user.id }, updates, { new: true, runValidators: true });

            if (!result) {
                return res.status(404).json({ message: 'Task not found!' });
            }
            return res.status(200).json(result);
        } catch (e) {
            return res.status(500).json(e);
        }
    }

    async deleteTask(req, res) {
        try {
            const id = req.params.id;
            const user = req.user;
            const task = await Task.findOneAndDelete({ _id: id, user: user.id });
            if (!task) {
                return res.status(404).json({ message: 'Task not found!' });
            }
            return res.status(200).json(task);
        } catch (e) {
            return res.status(500).json(e);
        }
    }

}

module.exports = new taskController();