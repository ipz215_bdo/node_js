const { Schema, model } = require('mongoose');

const Token = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    accessToken: { type: String, required: true },
});

module.exports = model('Token', Token);