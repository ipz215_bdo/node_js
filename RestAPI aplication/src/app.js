const express = require('express');
const mongoose = require('mongoose');
const createError = require('http-errors');
const userRouter = require('./Routes/User.route');
const taskRouter = require('./Routes/Task.route');
require('dotenv').config();

let url = process.env.MONGO_URL;
let PORT = process.env.PORT || 3000;

const app = express();

app.use(express.json());
/* app.use(express.urlencoded({ extended: true })); */
app.use('/users', userRouter);
app.use('/tasks', taskRouter);

const start = async () => {
    try {
        await mongoose.connect(url);
        app.listen(PORT, () => {
            console.log(`Server is up on port ${PORT}.`);
        });
        console.log('MongoDB connected');
    } catch (error) {
        console.log(error.message);
    }

    app.use((req, res, next) => {
        next(createError(404, 'Not found'));
    });

    app.use((err, req, res, next) => {
        res.status(err.status || 500);
        res.json({
            error: {
                status: err.status || 500,
                message: err.message
            }
        });
    });
}

start();