const simpleTask = (num) => {
    console.log("Завдяння 1");
    let num1 = Math.floor(num / 10);
    let num2 = num % 10;
    console.log(`Число ${num} розбите на цифри ${num1} та ${num2}\n`);
}

simpleTask(24);

const pies = (a, b, n) => {
    console.log("Завдяння 7336");
    let sum = (a * 100 + b) * n;
    let grn = Math.floor(sum / 100);
    let kop = sum % 100;
    console.log(`Петрик заплатить ${grn} грн. та ${kop} коп.\n`);
}

pies(1, 25, 2);

const Decomposition = (num) => {
    console.log("Завдяння 935");
    if (num < 0) {
        num = num * -1;
    }

    let num1 = Math.floor(num / 100);
    let num2 = Math.floor(num / 10) % 10;
    let num3 = num % 10;
    console.log(`Число ${num} розбите на цифри: \n${num1}\n${num2}\n${num3}\n`);
}

Decomposition(198);
Decomposition(-232);


const SpiralMatrix = (n, i, j) => {
    console.log("Завдяння 85");
    const matrix = new Array(n).fill(0).map(() => new Array(n).fill(0));

    let value = 1;

    let startRow = 0;
    let endRow = n - 1;
    let startCol = 0;
    let endCol = n - 1;

    while (startRow <= endRow && startCol <= endCol) {
        // Вправо
        for (let i = startCol; i <= endCol; i++) {
            matrix[startRow][i] = value;
            value++;
        }
        startRow++;

        // Вниз
        for (let i = startRow; i <= endRow; i++) {
            matrix[i][endCol] = value;
            value++;
        }
        endCol--;

        // Вліво
        for (let i = endCol; i >= startCol; i--) {
            matrix[endRow][i] = value;
            value++;
        }
        endRow--;

        // Вгору
        for (let i = endRow; i >= startRow; i--) {
            matrix[i][startCol] = value;
            value++;
        }
        startCol++;
    }

    console.log(matrix);

    console.log(`Число, яке знаходиться в ${i} рядку та ${j} стовпці: ${matrix[i - 1][j - 1]}\n`);
}

SpiralMatrix(5, 4, 2);


function calculatePossibleProducts(a, b, c, d) {
    console.log("Завдяння 916");
    let products = new Set();

    for (let i = a; i <= b; i++) {
        for (let j = c; j <= d; j++) {
            products.add(i * j);
        }
    }

    console.log(`Кількість можливих варіантів добутку: ${products.size}\n`);
}

calculatePossibleProducts(1, 10, 1, 10);

function printArray(arr) {
    const rows = arr.map(row => JSON.stringify(row)).join('\n');
    console.log(rows);
}

const half = (n) => {
    console.log("Завдяння 2666");
    const arr = [];

    for (let i = 0; i < n; i++) {
        arr[i] = [];

        for (let j = 0; j < n; j++) {
            if (i === j) {
                arr[i][j] = 0;
            } else if (j > i) {
                arr[i][j] = 2;
            } else {
                arr[i][j] = 1;
            }
        }
    }

    printArray(arr);
}

half(3);
console.log("\n");

const phoneNumber = (num) => {
    console.log("Завдяння 930");

    const digitSet = new Set("0123456789");
    const numberSet = new Set(num);

    const missingDigits = Array.from(digitSet).filter(digit => !numberSet.has(digit));
    const count = missingDigits.length;

    return [count, missingDigits.sort().join(" ")];
}

const number = "+380 63 4423122";
const [count, missingDigits] = phoneNumber(number);

console.log(count);
console.log(missingDigits);
console.log("\n");


const isPolindrom = (str) => {
    console.log("Завдяння 2162");
    let str1 = str.replace(/\s/g, '');
    let str2 = str1.split('').reverse().join('');

    if (str1 === str2) {
        console.log("YES\n");
    } else {
        console.log("NO\n");
    }
}

isPolindrom("a roza upala na lapu azora");

const countWords = (str) => {
    console.log("Завдяння 909");

    return `Кількість слів: ${str.split(/\s+/).length}\n`;
}

console.log(countWords("Hello world! Hello,    country!"));

function calculateMinDistance(n, k, coordinates) {

    console.log("Завдяння 4035");

    let left = 0;
    let right = coordinates[n - 1] - coordinates[0];
    let result = 0;

    while (left <= right) {
        const mid = Math.floor((left + right) / 2);

        let count = 1;
        let prevCoordinate = coordinates[0];

        for (let i = 1; i < n; i++) {
            if (coordinates[i] - prevCoordinate >= mid) {
                count++;
                prevCoordinate = coordinates[i];
            }
        }

        if (count >= k) {
            result = mid;
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    }

    return result;
}

const minDistance = calculateMinDistance(5, 3, [1, 2, 3, 100, 1000]);
console.log(minDistance);




function findLeaderCandidates(data) {
    data = data.toString();
    let n = parseInt(data.split(" ")[0]);
    let a = parseInt(data.split(" ")[1]);
    let max = 0;
    let num = 1;
    for(let i = 0; i < n; i++){ 
            
            if(a > max){
                max = a;
                num = 1;
            }
            else if(a == max){
                num++;
            }
        }
        console.log(parseInt(num));

}

findLeaderCandidates("5 1 2 3 4 5");