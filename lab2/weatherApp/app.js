const express = require('express');
const request = require('request');
const hbs = require("hbs");

const app = express();
const apiKey = '02694fe5d0acfbcd25cb9bcf1dc6009d';

hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine', 'hbs');
app.use(express.static('public'));

function getWeather(city, res) {
    const url = `http://api.openweathermap.org/data/2.5/weather?q=${encodeURIComponent(city)}&units=metric&appid=${apiKey}`;

    request(url, (error, response, body) => {
        const weather = JSON.parse(body);
        const icon = weather.weather[0].icon;
        const iconUrl = `http://openweathermap.org/img/wn/${icon}.png`;
        if (error || weather.cod === '404') {
            res.render('weather', {
                city,
                error: 'Unable to retrieve weather information'
            });
        } else {
            res.render('weather', {
                city: weather.name,
                temperature: weather.main.temp.toFixed(0),
                description: weather.weather[0].description,
                pressure: weather.main.pressure,
                humidity: weather.main.humidity,
                windSpeed: weather.wind.speed,
                windDirection: weather.wind.deg,
                maxTemp: weather.main.temp_max.toFixed(0),
                minTemp: weather.main.temp_min.toFixed(0),
                iconUrl,
                error: null
            });
        }
    });
}

app.get('/', (req, res) => {
    res.render('home', {
        title: 'Weather App',
    });
});

app.get('/login', (req, res) => {
    res.send('Login page');
});

app.get('/weather', (req, res) => {
    const city = req.query.city || 'Zhytomyr';
    getWeather(city, res);
});

app.get('/weather/:city', (req, res) => {
    const city = req.params.city || 'Zhytomyr';
    getWeather(city, res);
});

app.listen(3000, () => {
    console.log('Server is up on port 3000');
});