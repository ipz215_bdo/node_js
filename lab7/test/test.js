const factorial = require('../func/factorial.js');

var assert = require('assert');


describe('Function', function () {
    describe('factorial', function () {
        it('should return 120 when n = 5', function () {
            assert.equal(factorial.factorial(5), 120);
        });
        it('should return 6 when n = 720', function () {
            assert.equal(factorial.factorial(6), 720);
        });
        it('should return 0 when n = 1', function () {
            assert.equal(factorial.factorial(0), 1);
        });
        it('should return -5 when n = null', function () {
            assert.equal(factorial.factorial(-5), null);
        });
        it('should return -6 when n = null', function () {
            assert.equal(factorial.factorial(-6), null);
        });
    });
});

