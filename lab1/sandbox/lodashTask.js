const _lodash = require('lodash');


const array = [1, 2, 3];
const squares = _lodash.map(array, num => num * num);

console.log(squares);

const users = [
    { 'user': 'John', 'active': true },
    { 'user': 'Jane', 'active': false }
  ];
  const activeUsers = _lodash.filter(users, user => user.active);
  

    console.log(activeUsers);

    const users1 = [
        { 'user': 'John', 'age': 28 },
        { 'user': 'Jane', 'age': 24 },
        { 'user': 'Doe', age: 32 }
      ];
      const sortedUsers = _lodash.sortBy(users1, ['age']);
     

        console.log(sortedUsers);

    

        const doSearch = _lodash.debounce((query) => {
            
            console.log(`Searching for: ${query}`);
          }, 500);
          
          doSearch('cats');
          doSearch('dogs');
          
          

          const numbers = [1, 2, 1, 3, 2];
const uniqueNumbers = _lodash.uniq(numbers);


console.log(uniqueNumbers);