const fs = require('fs');
const path = require('path');

const filePath = path.join(__dirname, 'task1.txt');

fs.appendFile(filePath, 'Hello World!\n', (err) => {
    if (err) {
        console.log(err);
    }
});
