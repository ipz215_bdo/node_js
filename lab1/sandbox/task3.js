const fs = require('fs');
const path = require('path');

const os = require('os');
const userName = os.userInfo().username;

const filePath = path.join(__dirname, 'task2.txt');

fs.appendFile(filePath, `Hello, ${userName}!`, (err) => {
    if (err) {
        console.log(err);
    }
});
