const fs = require("fs");

function add(language) {
    //Прочитати файл user.json
    let userJSON = fs.readFileSync("user.json", "utf-8");
    //Перетворити дані з файлу в JS-обєкт
    let user = JSON.parse(userJSON);

    if (user.languages.find((l) => l.title === language.title)) {
        console.log("Language already exists");
        return;
    }

    //Отримати масив мов і додати новий елемент
    user.languages.push(language);

    //Перетворити JS-обєкт в JSON
    //Записати новий JSON у файл user.json
    fs.writeFileSync("user.json", JSON.stringify(user));
}

function remove(title) {
    let userJSON = fs.readFileSync("user.json", "utf-8");
    let user = JSON.parse(userJSON);
    let newLanguages = user.languages.filter((language) => {
        return language.title !== title;
    });
    user.languages = newLanguages;
    fs.writeFileSync("user.json", JSON.stringify(user));
}

function list() {
    let userJSON = fs.readFileSync("user.json", "utf-8");
    let user = JSON.parse(userJSON);
    console.log(user.languages);
}

function read(title) {
    let userJSON = fs.readFileSync("user.json", "utf-8");
    let user = JSON.parse(userJSON);
    let language = user.languages.find((language) => {
        return language.title === title;
    });
    console.log(language);
}

module.exports = {
    add,
    remove,
    list,
    read
};